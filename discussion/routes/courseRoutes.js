const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")

/* 
Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.

*/


router.post("/addcourse", auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)

    console.log(userData)

	courseController.addCourse( req.body, userData ).then(result => res.send(result))
} )



module.exports = router