/* const mongoose = require ("mongoose")

const userSchema = new mongoose.Schema ({
    
    firstName:{
        type:String,
        required:[true, "Please input your First Name"]
    },

    lastName:{
        type:String,
        required:[true, "Please input your Last Name"]
    },

    age:{
        type:Number,
        required:[true, "Please input your Age"]
    },

    gender:{
        type:String,
        required:[true, "Please input your gender"]
    },

    email:{
        type:String,
        required:[true, "Please input your email"]
    },

    password:{
        type:Number,
        required:[true, "Password is required"]
    },

    mobileNo:{
        type:Number,
        required:[true, "Please input your Mobile Number"]
    },

    isAdmin : {
        type : Boolean,
        default : false
    },

    enrollments : [
        {
            courseId:{
                type:String,
                required:[true, "please input Course ID"]
            },

            enrolledAt:{
                type:String,
                required:[true, "location is required"]
            },

            enrolledOn:{
                type:Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Enrolled"
            }
        }
    ]
})

module.exports = mongoose.model("User", userSchema) */


const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	age:{
		type: String,
		required: [true, "Age is required"]
	},
	gender: {
		type: String,
		required: [true, "Gender is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
    },
    password:{
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
	{
		courseId:{
			type: String,
			required: [true, "Course ID is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		},
		// to see if the student is still enrolled or not (graduated/dropped) in the course
		status: {
			type: String,
			default: "Enrolled"
		}
	}
	]
})


module.exports = mongoose.model("User", userSchema);